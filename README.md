# dotfiles

## Hyprland
- Blueman and nm-applet for bluetooth and network management
- Keyboard per window layout - [hyprland-per-window-layout](https://github.com/coffebar/hyprland-per-window-layout/)
- notifications - [dunst](https://github.com/dunst-project/dunst)
- wallpaper - [hyprpaper](https://github.com/hyprwm/hyprpaper)
- GTK theme - [Arc Dark](https://github.com/horst3180/arc-theme)
- Icon theme - [Qogir](https://github.com/vinceliuice/Qogir-icon-theme)

